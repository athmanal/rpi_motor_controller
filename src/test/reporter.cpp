/**
 * Client program for utilizing the RPi motor controller. It makes use of the DACs, ENCs, and TCP
 * servers available to it. It is designed for complete remote (this side) operation, so this API
 * exports the functions necessary to communicate with the server.
 * (c) Hari Ganti, 2013 <hariganti@gmail.com>
 */

#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>

#include "../include/tcp.h"


//As is, this program will no longer work. See remote_test.cpp
int main(int argc, char *argv[]) {
	//Declarations
	TCP tcp;
	std::string str;
	unsigned char ch[2];
	int data[3];

	memset(ch, 0, sizeof(ch));
	memset(data, 0, sizeof(data));

	//Initialization
	std::cout << "Enter the hostname :";
	std::cin >> str;
	tcp.connectToServer(str.c_str());

	//Receive waiting signal
	tcp.recvData(ch, 1);
	printf("Received %.2X\n", ch[0]);

	//Send mode signal
	ch[0] = 0x10;
	ch[1] = 0x00;
	tcp.sendData(ch, 2);
	printf("Sent %.2X %.2X\n", ch[0], ch[1]);

	//Receive waiting signal
	tcp.recvData(ch, 1);
	printf("Received %.2X\n", ch[0]);

	//Send start signal
	ch[0] = 0xAA;
	ch[1] = 0x00;
	tcp.sendData(ch, 2);
	printf("Sent %.2X %.2X\n", ch[0], ch[1]);

	//Send command signal
	ch[0] = 0x3F;
	ch[1] = 0xFF;
	tcp.sendData(ch, 2);
	printf("Sent %.2X %.2X\n", ch[0], ch[1]);

	//Send no-op/"over" signal
	ch[0] = 0x00;
	ch[1] = 0x00;
	tcp.sendData(ch, 2);
	printf("Sent %.2X %.2X\n", ch[0], ch[1]);

	//Receive data
	tcp.recvData(data, 3);
	printf("Received %d %d %d\n", data[0], data[1], data[2]);

	//Send shutdown signal
	ch[0] = 0xFF;
	ch[1] = 0x00;
	tcp.sendData(ch, 2);

	return 0;
}
