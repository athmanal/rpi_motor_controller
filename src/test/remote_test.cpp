#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>

#include "../include/remote.h"

int main(int argc, char *argv[]) {
	//Initialization
	bool run = true;
	char str[2];
	uint16_t codes[3];
	int *data = new int[3];
	timeval start, stop;

	Remote *remote = new Remote("BFRPi", 0);
	remote->start();
	printf("Initialization complete\n\n");

	//Print instructions
	printf("The commands in use are:\n"
				"h - help\n"
				"e - run experiment\n"
				"s - set DACs and read ENCs\n"
				"r - read ENCs\n"
				"x - exit\n");

	//Begin main loop
	while(run) {
		printf("Enter a command: ");
		scanf("%s", str);

		switch(str[0]) {
		  case 'h':
			printf("The commands in use are:\n"
					"h - help\n"
					"e - run experiment\n"
					"s - set DACs and read ENCs\n"
					"r - read ENCs\n"
					"x - exit\n");
		  break;

		  case 'e':
		  	printf("Beginning a time trial!\n");
		  	codes[2] = 0;
		  	for(uint16_t i = 0; i < 4096; i++) {
				printf("%hu\n", i);
				codes[0] = codes[1] = i;
		  		gettimeofday(&start, NULL);
		  		remote->readWrite(codes, data);
		  		gettimeofday(&stop, NULL);

				printf("Call time: %ld\n", (long)(1e6 * (stop.tv_sec - start.tv_sec))  + (long)(stop.tv_usec - start.tv_usec));
//				usleep(50000);
		  	}
		   break;

		  case 's':
		  	for(int i = 0; i < 3; i++) {
		  		printf("Enter the value for channel %d: ", i);
		  		scanf("%hu", &(codes[i]));
		  	}

			remote->readWrite(codes, data);
		  	printf("Received: %d %d %d\n", data[0], data[1], data[2]);
		  break;

		  case 'r':
			codes[0] = codes[1] = codes[2] = 0;
		  	remote->readWrite(codes, data);
		  	printf("Received: %d %d %d\n", data[0], data[1], data[2]);
		  break;

		  case 'x':
//		  	remote->resetDAC();
//		  	remote->resetENC();
		  	remote->shutdown();

		  	delete[] data;
		  	return 0;
		  break;

		  default:
		  	printf("Unrecognized command\n");
		  break;
		}
	}

	return 0;
}
