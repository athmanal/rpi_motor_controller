/**
 * Client header for utilizing the RPi motor controller. It makes use of the DACs, ENCs, and TCP
 * servers available to it. It is designed for complete remote (this side) operation, so this API
 * exports the functions necessary to communicate with the server. It itself does not print data.
 * (c) Hari Ganti, 2013 <hariganti@gmail.com>
 */

//Includes
#include <stdint.h>

#include "tcp.h"

//Constants
//Operating modes
static unsigned char NO_OP		= 0x00;
static unsigned char MODE_3M3E	= 0x10;
static unsigned char MODE_3M6E	= 0x11;
static unsigned char MODE_6M6E	= 0x12;
static unsigned char MODE_6M12E	= 0x13;
//Runtime signals
static unsigned char CMD_START		= 0xAA;
static unsigned char CMD_RW			= 0x55;
static unsigned char CMD_RST_DAC	= 0xDD;
static unsigned char CMD_RST_ENC	= 0xEE;
static unsigned char CMD_SHDN		= 0xFF;

class Remote {
	TCP tcp;
  	unsigned char *cmdBuf;
  	int cmdlen;
  	int datlen;
  public:
  	Remote(const char *hostname, int mode);
  	~Remote();
  	void start();
  	void shutdown();
  	void resetDAC();
  	void resetENC();
  	void readWrite(uint16_t *codes, int *data);
};
