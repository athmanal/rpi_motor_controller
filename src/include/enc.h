/**
 * Class for reading the iC-MD encoder (ENC)
 * (c) Hari Ganti, 2013 <hariganti@gmail.com>
 */

//Includes
#include <stdio.h>

//wiringPi Libraries
#include <wiringPi.h>
#include <wiringPiSPI.h>

//Read / Write Bit
#define RD 0b10000000
#define WR 0b00000000

//Addresses
#define CNT_ADDR 0b00000000 //0x00 - Counter configuration
#define TTL_ADDR 0b00000001 //0x01 - TTL configuration
#define VAL_ADDR 0b00001000 //0x08 - Counter read
#define RST_ADDR 0b00110000 //0x30 - Instruction byte (reset counters)

//Commands
#define CNT_CFG 0b00000111 //INVZ1 INVZ0 EXCH2 EXCH1 EXCH0 CNTCFG(2:0)
#define TTL_CFG 0b10000111 //TTL CBZ1 CBZ0 CFGZ(4:3) TPCFG(2:1) PRIOR
#define RST_CMD 0b00000111 //[RSV] ACT1 ACT0 TP ZCEN ABRES2 ABRES1 ABRES0

class ENC {
	int pin;
	void transmit(unsigned char *ch, int len);
  public:
  	ENC();
  	ENC(int pin_in);
  	~ENC();
  	void setPin(int pin_in);
  	bool reset();
  	void readENC(unsigned char *buf);
};
