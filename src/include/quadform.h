/**
 * Class for generating a quadrature waveform
 * (c) Hari Ganti, 2013 <hariganti@gmail.com>
 */

//Includes
#include <wiringPi.h>

//Signal Pins
 #define A 0
 #define B 1

class QuadForm {
	unsigned int uSecs;
  public:
  	QuadForm();
  	QuadForm(unsigned int uSecs_in);
  	void generate();
};
