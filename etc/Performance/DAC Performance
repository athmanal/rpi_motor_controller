DAC Performance
		
Notes:
	Likely limited by the need to lock and unlock the SPI communications. Tests were also done "w/o locking," which is not safe, since the communication lines are shared and locking prevents cross-talk, which has caused problems previously

	Jumping large distances tends to caused inductive effects. For example, the voltage peaked at -10.2 volts from -9.6 volts after receiving a jump command from 0 to 4095

	Noise is generally the same regardless of output voltage, so the SNR decreases as the output decreases. Noise should also decrease with a smaller reference voltage (more suited to the output scale), so an adjustable voltage reference is suggested

Tests:

	Jump
		The DAC is commanded to jump from one command code to another as quickly as possible
			Slowest jump (0 to 4095)
				4.16ms
				3.80ms w/o locking
			Intermediate jump (1024 to 3072)
			Fastest jump (2047 to 2049)

	Estimated noise
		The DAC is commanded to oscillate between two codes, and hold each for 500ms
			Largest oscillation (0 to 4095)
			Intermediate oscillation (1024 to 3072)
			Smallest oscillation (2047 to 2049)

	Control Loop Rise time
		The DAC is commanded to ramp up from one code to another as quickly as possible
			Slowest ramp (0 to 4095)
				612ms => 6.69kHz or 150 µs/n
				516ms => 7.93kHz or 126 µs/n w/o locking

	Step-function (continuity)
		The DAC is commanded to walk up and down through its range, pausing for 5ms at each step